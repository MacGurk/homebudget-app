import { useEffect, useState } from 'react';
import axios from 'axios';

axios.defaults.baseURL = 'http://localhost:3000';

type GetResponse<T> = { response: T | undefined | null; error?: string; loading?: boolean };

const useAPI = <T>(apiPath: string): GetResponse<T> => {
  const [response, setResponse] = useState<T | undefined | null>(null);
  const [error, setError] = useState<string>('');
  const [loading, setLoading] = useState<boolean>(true);

  const fetchData = () => {
    axios
      .get(apiPath)
      .then((res) => {
        setResponse(res.data);
      })
      .catch((err) => {
        setError(err);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    fetchData();
  }, []);

  return { response, error, loading };
};
