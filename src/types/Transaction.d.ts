import { User } from './User';

export interface Transaction {
  id: string;
  date: Date;
  description: string;
  price: number;
  settled: boolean;
  user: User;
}
