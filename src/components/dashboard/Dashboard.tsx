import React from 'react';
import Title from '../Title';

function Dashboard() {
  return (
    <div>
      <Title title="Dashboard" />
    </div>
  );
}

export default Dashboard;
