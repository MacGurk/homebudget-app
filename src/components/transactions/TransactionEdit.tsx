import React from 'react';
import { useParams } from 'react-router-dom';
import TransactionForm from './TransactionForm';

function TransactionEdit() {
  const { id } = useParams();

  return (
    <div>
      <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center mb-3 pt-3 pb-2 px-3 border-bottom">
        <h2>Transactions</h2>
      </div>
      <div className="mx-3">
        <TransactionForm id={id} />
      </div>
    </div>
  );
}

export default TransactionEdit;
