import React from 'react';
import TransactionCards from './cards/TransactionCards';
import TransactionsTable from './table/TransactionsTable';
import Title from '../Title';
import { Transaction } from '../../types/Transaction';

const Transactions: React.FC = () => {
  const dummyData: Transaction[] = [
    {
      id: '1',
      date: new Date(),
      description: 'description',
      price: 10,
      settled: false,
      user: {
        id: '1',
        username: 'username',
        email: 'asdfasdf',
      },
    },
    {
      id: '2',
      date: new Date(),
      description: 'description2',
      price: 20,
      settled: false,
      user: {
        id: '2',
        username: 'username2',
        email: 'asdfasdf2',
      },
    },
  ];

  return (
    <div>
      <Title title="Transactions" />
      {window.screen.availWidth < 512 ? (
        <TransactionCards transactions={dummyData} />
      ) : (
        <TransactionsTable transactions={dummyData} />
      )}
    </div>
  );
};

export default Transactions;
