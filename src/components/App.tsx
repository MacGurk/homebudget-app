import React from 'react';
import { Route, Routes } from 'react-router-dom';
import Login from './login/Login';
import Content from './Content';
import NoMatch from './misc/NoMatch';
import Dashboard from './dashboard/Dashboard';
import Transactions from './transactions/Transactions';
import User from './user/User';

const App: React.FC = () => (
  <Routes>
    <Route path="/" element={<Content />}>
      <Route index element={<Dashboard />} />
      <Route path="transactions" element={<Transactions />} />
      <Route path="user" element={<User />} />
      <Route path="*" element={<NoMatch />} />
    </Route>
    <Route path="/login" element={<Login />} />
    <Route path="*" element={<NoMatch />} />
  </Routes>
);

export default App;
